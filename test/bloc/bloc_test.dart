import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:harry_potter/bloc/bloc.dart';

void main() {
  blocTest<BlocVerificacion, Estado>(
    'Cuando se añade evento creado, muestra menu',
    build: () => BlocVerificacion(),
    act: (bloc) => bloc.add(Creado()),
    expect: () => [isA<MostrandoMenu>()],
  );
  blocTest<BlocVerificacion, Estado>(
    'Cuando la opcion es personajes muestra todos los personajes de Harry Potter.',
    build: () => BlocVerificacion(),
    seed: () => MostrandoMenu(),
    act: (bloc) => bloc.add(OpcionRecibida('personajes')),
    expect: () => [isA<EsperandoInformacion>()],
  );
  blocTest<BlocVerificacion, Estado>(
    'Cuando la opcion es estudiantes muestra todos los estudiantes de Hogwarts durante la serie de libros',
    build: () => BlocVerificacion(),
    seed: () => MostrandoMenu(),
    act: (bloc) => bloc.add(OpcionRecibida('estudiantes')),
    expect: () => [isA<EsperandoInformacion>()],
  );
  blocTest<BlocVerificacion, Estado>(
    'Cuando la opcion es personal muestra todo el personal de Hogwarts durante la serie de libros.',
    build: () => BlocVerificacion(),
    seed: () => MostrandoMenu(),
    act: (bloc) => bloc.add(OpcionRecibida('personal')),
    expect: () => [isA<EsperandoInformacion>()],
  );
  blocTest<BlocVerificacion, Estado>(
    'Cuando la opcion es porcasa muestra menu de casas.',
    build: () => BlocVerificacion(),
    seed: () => MostrandoMenu(),
    act: (bloc) => bloc.add(OpcionRecibida('porcasa')),
    expect: () => [isA<EsperandoInformacion>(),
                  isA<MostrandoMenuCasas>()],
  );
  blocTest<BlocVerificacion, Estado>(
    'Cuando la opcion es una casa muestra personajespor la casa.',
    build: () => BlocVerificacion(),
    seed: () => MostrandoMenu(),
    act: (bloc) => bloc.add(OpcionCasaRecibida('casa-gryffindor')),
    expect: () => [isA<EsperandoInformacion>()],
  );
  blocTest<BlocVerificacion, Estado>(
    'Cuando la opcion es hechizos muestra todos los hechizos.',
    build: () => BlocVerificacion(),
    seed: () => MostrandoMenu(),
    act: (bloc) => bloc.add(OpcionRecibida('hechizos')),
    expect: () => [isA<EsperandoInformacion>()],
  );
}