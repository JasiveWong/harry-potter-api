import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:harry_potter/dominio/personaje.dart';
import 'package:harry_potter/dominio/problemas.dart';
import 'package:harry_potter/repositorio/repositorio_personaje.dart';

void main() {
  test('Hay 403 personajes offline', (){
    String json = File('lib/json/personajes.json').readAsStringSync();
    final respuesta = obtenerListaPersonajes(json);
    respuesta.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.length, equals(403));
    });
  });

  test('Harry esta en la lista de personajes offline', (){
    String json = File('lib/json/personajes.json').readAsStringSync();
    final respuesta = obtenerListaPersonajes(json);
    final harry = Personaje(nombre: "Harry Potter", nombresAlternativos: [], especie: 'human', genero: 'male', casa: 'Gryffindor', fechaNacimiento: '31-07-1980', anio: 1980, esMago: true, ascendencia: 'half-blood', colorOjos: 'green', colorPelo: 'black', patronus: 'stag', estudianteHogwarts: true, personalHogwarts: false, actor: 'Daniel Radcliffe', actoresAlternativos: [], vivo: true, imagen: 'https://hp-api.herokuapp.com/images/harry.jpg');
    respuesta.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r[0].nombre.contains(harry.nombre), equals(true));
    });
  });

  test('Si mando json de hechizos manda error', (){
    String json = File('lib/json/hechizos.json').readAsStringSync();
    final respuesta = obtenerListaPersonajes(json);
    respuesta.match((l){
      expect(l, isA<JsonMalformado>());
    }, (r){
      assert(false);
    });
  });
}