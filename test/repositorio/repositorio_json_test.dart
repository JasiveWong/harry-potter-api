import 'package:flutter_test/flutter_test.dart';
import 'package:harry_potter/repositorio/repositorio_json.dart';

void main() {
  test('si la opcion es personajes devuelve string de personajes offline', (){
    RepositorioOfflineJson repositorio = RepositorioOfflineJson();
    final resp = repositorio.obtenerjson('personajes');
    String informacionHarry= '{"name":"Harry Potter","alternate_names":[],"species":"human","gender":"male","house":"Gryffindor","dateOfBirth":"31-07-1980","yearOfBirth":1980,"wizard":true,"ancestry":"half-blood","eyeColour":"green","hairColour":"black","wand":{"wood":"holly","core":"phoenix feather","length":11},"patronus":"stag","hogwartsStudent":true,"hogwartsStaff":false,"actor":"Daniel Radcliffe","alternate_actors":[],"alive":true,"image":"https://hp-api.herokuapp.com/images/harry.jpg"}';
    resp.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(informacionHarry), equals(true));
    });
  });

  test('si la opcion es estudiantes devuelve string de estudiantes offline', (){
    RepositorioOfflineJson repositorio = RepositorioOfflineJson();
    final resp = repositorio.obtenerjson('estudiantes');
    String informacionHarry= '{"name":"Harry Potter","alternate_names":[],"species":"human","gender":"male","house":"Gryffindor","dateOfBirth":"31-07-1980","yearOfBirth":1980,"wizard":true,"ancestry":"half-blood","eyeColour":"green","hairColour":"black","wand":{"wood":"holly","core":"phoenix feather","length":11},"patronus":"stag","hogwartsStudent":true,"hogwartsStaff":false,"actor":"Daniel Radcliffe","alternate_actors":[],"alive":true,"image":"https://hp-api.herokuapp.com/images/harry.jpg"}';
    resp.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(informacionHarry), equals(true));
    });
  });

  test('si la opcion es personal devuelve string de personal offline', (){
    RepositorioOfflineJson repositorio = RepositorioOfflineJson();
    final resp = repositorio.obtenerjson('personal');
    String informacionMinerva= '{"name":"Minerva McGonagall","alternate_names":[],"species":"human","gender":"female","house":"Gryffindor","dateOfBirth":"04-10-1925","yearOfBirth":1925,"wizard":true,"ancestry":"","eyeColour":"","hairColour":"black","wand":{"wood":"","core":"","length":null},"patronus":"tabby cat","hogwartsStudent":false,"hogwartsStaff":true,"actor":"Dame Maggie Smith","alternate_actors":[],"alive":true,"image":"https://hp-api.herokuapp.com/images/mcgonagall.jpg"}';
    resp.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(informacionMinerva), equals(true));
    });
  });

  test('si la opcion es casa-gryffindor devuelve string de casa Gryffindor offline', (){
    RepositorioOfflineJson repositorio = RepositorioOfflineJson();
    final resp = repositorio.obtenerjson('casa-gryffindor');
    String informacionHarry= '{"name":"Harry Potter","alternate_names":[],"species":"human","gender":"male","house":"Gryffindor","dateOfBirth":"31-07-1980","yearOfBirth":1980,"wizard":true,"ancestry":"half-blood","eyeColour":"green","hairColour":"black","wand":{"wood":"holly","core":"phoenix feather","length":11},"patronus":"stag","hogwartsStudent":true,"hogwartsStaff":false,"actor":"Daniel Radcliffe","alternate_actors":[],"alive":true,"image":"https://hp-api.herokuapp.com/images/harry.jpg"}';
    resp.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(informacionHarry), equals(true));
    });
  });

  test('si la opcion es casa-slytherin devuelve string de casa Slytherin offline', (){
    RepositorioOfflineJson repositorio = RepositorioOfflineJson();
    final resp = repositorio.obtenerjson('casa-slytherin');
    String informacionDraco= '{"name":"Draco Malfoy","alternate_names":[],"species":"human","gender":"male","house":"Slytherin","dateOfBirth":"05-06-1980","yearOfBirth":1980,"wizard":true,"ancestry":"pure-blood","eyeColour":"grey","hairColour":"blonde","wand":{"wood":"hawthorn","core":"unicorn tail-hair","length":10},"patronus":"","hogwartsStudent":true,"hogwartsStaff":false,"actor":"Tom Felton","alternate_actors":[],"alive":true,"image":"https://hp-api.herokuapp.com/images/draco.jpg"}';
    resp.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(informacionDraco), equals(true));
    });
  });

  test('si la opcion es casa-hufflepuff devuelve string de casa hufflepuff offline', (){
    RepositorioOfflineJson repositorio = RepositorioOfflineJson();
    final resp = repositorio.obtenerjson('casa-hufflepuff');
    String informacionCendric= '{"name":"Cedric Diggory","alternate_names":[],"species":"human","gender":"male","house":"Hufflepuff","dateOfBirth":"","yearOfBirth":1977,"wizard":true,"ancestry":"","eyeColour":"grey","hairColour":"brown","wand":{"wood":"ash","core":"unicorn hair","length":12.25},"patronus":"","hogwartsStudent":true,"hogwartsStaff":false,"actor":"Robert Pattinson","alternate_actors":[],"alive":false,"image":"https://hp-api.herokuapp.com/images/cedric.png"}';
    resp.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(informacionCendric), equals(true));
    });
  });

  test('si la opcion es casa-ravenclaw devuelve string de casa Ravenclaw offline', (){
    RepositorioOfflineJson repositorio = RepositorioOfflineJson();
    final resp = repositorio.obtenerjson('casa-ravenclaw');
    String informacionCho= '{"name":"Cho Chang","alternate_names":[],"species":"human","gender":"female","house":"Ravenclaw","dateOfBirth":"","yearOfBirth":null,"wizard":true,"ancestry":"","eyeColour":"brown","hairColour":"black","wand":{"wood":"","core":"","length":null},"patronus":"swan","hogwartsStudent":true,"hogwartsStaff":false,"actor":"Katie Leung","alternate_actors":[],"alive":true,"image":"https://hp-api.herokuapp.com/images/cho.jpg"}';
    resp.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(informacionCho), equals(true));
    });
  });

  test('si la opcion es hechizos devuelve string de hechizos offline', (){
    RepositorioOfflineJson repositorio = RepositorioOfflineJson();
    final resp = repositorio.obtenerjson('hechizos');
    String informacionAberto= '{"name":"Aberto","description":"Opens locked doors"}';
    resp.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(informacionAberto), equals(true));
    });
  });

  test('si la opcion es nada devuelve String vacio offline', (){
    RepositorioOfflineJson repositorio = RepositorioOfflineJson();
    final resp = repositorio.obtenerjson('nada');
    String informacion= '';
    resp.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(informacion), equals(true));
    });
  });

  test('si la opcion es personajes devuelve string de personajes online', ()async{
    RepositorioOnlineJson repositorio = RepositorioOnlineJson();
    final resp = await repositorio.obtenerjson('personajes');
    String informacionHarry= '{"name":"Harry Potter","alternate_names":[],"species":"human","gender":"male","house":"Gryffindor","dateOfBirth":"31-07-1980","yearOfBirth":1980,"wizard":true,"ancestry":"half-blood","eyeColour":"green","hairColour":"black","wand":{"wood":"holly","core":"phoenix feather","length":11},"patronus":"stag","hogwartsStudent":true,"hogwartsStaff":false,"actor":"Daniel Radcliffe","alternate_actors":[],"alive":true,"image":"https://hp-api.herokuapp.com/images/harry.jpg"}';
    resp.match((l){
        expect(true, equals(false));
    }, (r){
        expect(r.contains(informacionHarry), equals(true));
    });
  });

  test('si la opcion es estudiantes devuelve string de estudiantes online', ()async{
    RepositorioOnlineJson repositorio = RepositorioOnlineJson();
    final resp = await repositorio.obtenerjson('estudiantes');
    String informacionHarry= '{"name":"Harry Potter","alternate_names":[],"species":"human","gender":"male","house":"Gryffindor","dateOfBirth":"31-07-1980","yearOfBirth":1980,"wizard":true,"ancestry":"half-blood","eyeColour":"green","hairColour":"black","wand":{"wood":"holly","core":"phoenix feather","length":11},"patronus":"stag","hogwartsStudent":true,"hogwartsStaff":false,"actor":"Daniel Radcliffe","alternate_actors":[],"alive":true,"image":"https://hp-api.herokuapp.com/images/harry.jpg"}';
    resp.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(informacionHarry), equals(true));
    });
  });

  test('si la opcion es personal devuelve string de personal online', ()async{
    RepositorioOnlineJson repositorio = RepositorioOnlineJson();
    final resp = await repositorio.obtenerjson('personal');
    String informacionMinerva= '{"name":"Minerva McGonagall","alternate_names":[],"species":"human","gender":"female","house":"Gryffindor","dateOfBirth":"04-10-1925","yearOfBirth":1925,"wizard":true,"ancestry":"","eyeColour":"","hairColour":"black","wand":{"wood":"","core":"","length":null},"patronus":"tabby cat","hogwartsStudent":false,"hogwartsStaff":true,"actor":"Dame Maggie Smith","alternate_actors":[],"alive":true,"image":"https://hp-api.herokuapp.com/images/mcgonagall.jpg"}';
    resp.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(informacionMinerva), equals(true));
    });
  });

  test('si la opcion es casa-gryffindor devuelve string de casa Gryffindor online', ()async{
    RepositorioOnlineJson repositorio = RepositorioOnlineJson();
    final resp = await repositorio.obtenerjson('casa-gryffindor');
    String informacionHarry= '{"name":"Harry Potter","alternate_names":[],"species":"human","gender":"male","house":"Gryffindor","dateOfBirth":"31-07-1980","yearOfBirth":1980,"wizard":true,"ancestry":"half-blood","eyeColour":"green","hairColour":"black","wand":{"wood":"holly","core":"phoenix feather","length":11},"patronus":"stag","hogwartsStudent":true,"hogwartsStaff":false,"actor":"Daniel Radcliffe","alternate_actors":[],"alive":true,"image":"https://hp-api.herokuapp.com/images/harry.jpg"}';
    resp.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(informacionHarry), equals(true));
    });
  });

  test('si la opcion es casa-slytherin devuelve string de casa Slytherin online', ()async{
    RepositorioOnlineJson repositorio = RepositorioOnlineJson();
    final resp = await repositorio.obtenerjson('casa-slytherin');
    String informacionDraco= '{"name":"Draco Malfoy","alternate_names":[],"species":"human","gender":"male","house":"Slytherin","dateOfBirth":"05-06-1980","yearOfBirth":1980,"wizard":true,"ancestry":"pure-blood","eyeColour":"grey","hairColour":"blonde","wand":{"wood":"hawthorn","core":"unicorn tail-hair","length":10},"patronus":"","hogwartsStudent":true,"hogwartsStaff":false,"actor":"Tom Felton","alternate_actors":[],"alive":true,"image":"https://hp-api.herokuapp.com/images/draco.jpg"}';
    resp.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(informacionDraco), equals(true));
    });
  });

  test('si la opcion es casa-hufflepuff devuelve string de casa hufflepuff online', ()async{
    RepositorioOnlineJson repositorio = RepositorioOnlineJson();
    final resp = await repositorio.obtenerjson('casa-hufflepuff');
    String informacionCendric= '{"name":"Cedric Diggory","alternate_names":[],"species":"human","gender":"male","house":"Hufflepuff","dateOfBirth":"","yearOfBirth":1977,"wizard":true,"ancestry":"","eyeColour":"grey","hairColour":"brown","wand":{"wood":"ash","core":"unicorn hair","length":12.25},"patronus":"","hogwartsStudent":true,"hogwartsStaff":false,"actor":"Robert Pattinson","alternate_actors":[],"alive":false,"image":"https://hp-api.herokuapp.com/images/cedric.png"}';
    resp.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(informacionCendric), equals(true));
    });
  });

  test('si la opcion es casa-ravenclaw devuelve string de casa Ravenclaw online', ()async{
    RepositorioOnlineJson repositorio = RepositorioOnlineJson();
    final resp = await repositorio.obtenerjson('casa-ravenclaw');
    String informacionCho= '{"name":"Cho Chang","alternate_names":[],"species":"human","gender":"female","house":"Ravenclaw","dateOfBirth":"","yearOfBirth":null,"wizard":true,"ancestry":"","eyeColour":"brown","hairColour":"black","wand":{"wood":"","core":"","length":null},"patronus":"swan","hogwartsStudent":true,"hogwartsStaff":false,"actor":"Katie Leung","alternate_actors":[],"alive":true,"image":"https://hp-api.herokuapp.com/images/cho.jpg"}';
    resp.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(informacionCho), equals(true));
    });
  });

  test('si la opcion es hechizos devuelve string de hechizos online', ()async{
    RepositorioOnlineJson repositorio = RepositorioOnlineJson();
    final resp = await repositorio.obtenerjson('hechizos');
    String informacionAberto= '{"name":"Aberto","description":"Opens locked doors"}';
    resp.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(informacionAberto), equals(true));
    });
  });

  test('si la opcion es nada devuelve String vacio online', () async{
    RepositorioOnlineJson repositorio = RepositorioOnlineJson();
    final resp = await repositorio.obtenerjson('nada');
    String informacion= '';
    resp.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.contains(informacion), equals(true));
    });
  });
}