import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:harry_potter/dominio/hechizo.dart';
import 'package:harry_potter/dominio/problemas.dart';
import 'package:harry_potter/repositorio/repositorio_hechizo.dart';

void main() {
  test('Hay 77 hechizos offline', (){
    String json = File('lib/json/hechizos.json').readAsStringSync();
    final respuesta = obtenerListaHechizos(json);
    respuesta.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r.length, equals(77));
    });
  });

  test('Aberto esta en la lista de hechizos offline', (){
    String json = File('lib/json/hechizos.json').readAsStringSync();
    final respuesta = obtenerListaHechizos(json);
    final aberto = Hechizo(nombre: "Aberto", descripcion: "Opens locked doors");
    respuesta.match((l){
      expect(true, equals(false));
    }, (r){
      expect(r[0].nombre.contains(aberto.nombre), equals(true));
    });
  });

  test('Si mando json de personajes manda error', (){
    String json = File('lib/json/personajes.json').readAsStringSync();
    final respuesta = obtenerListaHechizos(json);
    respuesta.match((l){
      expect(l, isA<JsonMalformado>());
    }, (r){
      assert(false);
    });
  });
}