const String mensajeCampoVacio = 'Sin valor';

class Hechizo {
  final String nombre;
  final String descripcion;

  Hechizo({required this.nombre, required this.descripcion});

  Hechizo.fromJson(Map<String, dynamic> json)
    : nombre = json['name'].toString().isEmpty ? mensajeCampoVacio: json['name'],
      descripcion = json['description'].toString().isEmpty ? mensajeCampoVacio: json['description']; 
  
  Map<String, dynamic> toJson() => {
      'nombre': nombre,
      'descripcion': descripcion,
  };
}
