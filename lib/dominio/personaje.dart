const String mensajeCampoVacio = 'Sin valor';

class Personaje {
  final String nombre;
  final List<dynamic> nombresAlternativos;
  final String especie;
  final String genero;
  final String casa;
  final String fechaNacimiento;
  final int anio;
  final bool esMago;
  final String ascendencia;
  final String colorOjos;
  final String colorPelo;
  final String patronus;
  final bool estudianteHogwarts;
  final bool personalHogwarts;
  final String actor;
  final List<dynamic> actoresAlternativos;
  final bool vivo;
  final String imagen;
  
  Personaje({
    required this.nombre,
    required this.nombresAlternativos,
    required this.especie,
    required this.genero,
    required this.casa,
    required this.fechaNacimiento,
    required this.anio,
    required this.esMago,
    required this.ascendencia,
    required this.colorOjos,
    required this.colorPelo,
    required this.patronus,
    required this.estudianteHogwarts,
    required this.personalHogwarts,
    required this.actor,
    required this.actoresAlternativos,
    required this.vivo,
    required this.imagen,
  });

  Personaje.fromJson(Map<String, dynamic> json)
    : nombre = json['name'].toString().isEmpty ? mensajeCampoVacio: json['name'],
      nombresAlternativos = json['alternate_names'] ?? [],
      especie = json['species'].toString().isEmpty ? mensajeCampoVacio: json['species'],
      genero = json['gender'].toString().isEmpty ? mensajeCampoVacio: json['gender'],
      casa = json['house'].toString().isEmpty ? mensajeCampoVacio: json['house'],
      fechaNacimiento = json['dateOfBirth'].toString().isEmpty ? mensajeCampoVacio: json['dateOfBirth'],
      anio = json['yearOfBirth'] ?? 0,
      esMago = json['wizard'].toString().isEmpty ? mensajeCampoVacio: json['wizard'],
      ascendencia = json['ancestry'].toString().isEmpty ? mensajeCampoVacio: json['ancestry'],
      colorOjos = json['eyeColour'].toString().isEmpty ? mensajeCampoVacio: json['eyeColour'],
      colorPelo = json['hairColour'].toString().isEmpty ? mensajeCampoVacio: json['hairColour'],
      patronus = json['patronus'].toString().isEmpty ? mensajeCampoVacio: json['patronus'],
      estudianteHogwarts = json['hogwartsStudent'].toString().isEmpty ? mensajeCampoVacio: json['hogwartsStudent'],
      personalHogwarts = json['hogwartsStaff'].toString().isEmpty ? mensajeCampoVacio: json['hogwartsStaff'],
      actor = json['actor'].toString().isEmpty ? mensajeCampoVacio: json['actor'],
      actoresAlternativos = json['alternate_actors'] ?? [],
      vivo = json['alive'].toString().isEmpty ? mensajeCampoVacio: json['alive'],
      imagen = json['image'];

    Map<String, dynamic> toJson() => {
      'nombre': nombre,
      'nombresAlternativos': nombresAlternativos,
      'especie': especie,
      'genero': genero,
      'casa': casa,
      'fechaNacimiento': fechaNacimiento,
      'anio': anio,
      'esMago': esMago,
      'ascendencia': ascendencia,
      'colorOjos': colorOjos,
      'colorPelo': colorPelo,
      'patronus': patronus,
      'estudianteHogwarts': estudianteHogwarts,
      'personalHogwarts': personalHogwarts,
      'actor': actor,
      'actoresAlternativos': actoresAlternativos,
      'vivo': vivo,
      'imagen': imagen,
    };
}
