import 'dart:convert';

import 'package:fpdart/fpdart.dart';
import 'package:harry_potter/dominio/personaje.dart';
import 'package:harry_potter/dominio/problemas.dart';

Either<Problema,List<Personaje>> obtenerListaPersonajes(String json) {
  try {
    List<dynamic> personajes = jsonDecode(json);
    return Right(personajes.map((e) => Personaje.fromJson(e)).toList());
  } catch (e) {
    return Left(JsonMalformado());
  }
}