import 'dart:io';
import 'package:fpdart/fpdart.dart';
import 'package:harry_potter/dominio/problemas.dart';
import 'package:http/http.dart' as http;

class RepositorioOfflineJson{
  Either<Problema, String> obtenerjson (String opcion){
    String json='';
    switch (opcion) {
            case 'personajes':  try{
                                  json=File('lib/json/personajes.json').readAsStringSync();
                                  break;
                                }catch(e){
                                  return Left(ArchivoNoEncontrado());
                                }
            case 'estudiantes': try{
                                  json=File('lib/json/estudiantes.json').readAsStringSync();
                                  break;
                                }catch(e){
                                  return Left(ArchivoNoEncontrado());
                                }
            case 'personal':  try{
                                json=File('lib/json/personal.json').readAsStringSync();
                                break;
                              }catch(e){
                                return Left(ArchivoNoEncontrado());
                              }
            case 'casa-gryffindor': try{ 
                                      json=File('lib/json/casa_gryffindor.json').readAsStringSync();
                                      break;
                                    }catch(e){
                                      return Left(ArchivoNoEncontrado());
                                    }
            case 'casa-slytherin':try{ 
                                    json=File('lib/json/casa_slytheryn.json').readAsStringSync();
                                    break;
                                  }catch(e){
                                    return Left(ArchivoNoEncontrado());
                                  }
            case 'casa-hufflepuff': try{ 
                                      json=File('lib/json/casa_hufflepuff.json').readAsStringSync();
                                      break;
                                    }catch(e){
                                      return Left(ArchivoNoEncontrado());
                                    }
            case 'casa-ravenclaw':  try{ 
                                      json=File('lib/json/casa_ravenclaw.json').readAsStringSync();
                                      break;
                                    }catch(e){
                                      return Left(ArchivoNoEncontrado());
                                    }
            case 'hechizos': try{
                                json=File('lib/json/hechizos.json').readAsStringSync();
                                break;
                              }catch(e){
                                return Left(ArchivoNoEncontrado());
                              }
          }
    return Right(json);
  }
}

class RepositorioOnlineJson{
  Future<Either<Problema, String>> obtenerjson (String opcion) async {
    String json='';
    switch (opcion) {
            case 'personajes': try{
                                  Uri direccion = Uri.http('hp-api.onrender.com', 'api/characters');
                                  var respuesta = await http.get(direccion);
                                  json= respuesta.body;
                                  break;
                                }catch(e){
                                  return Left(ServidorNoAlcanzado());
                                }
            case 'estudiantes': try{
                                  Uri direccion = Uri.http('hp-api.onrender.com', 'api/characters/students');
                                  var respuesta = await http.get(direccion);
                                  json= respuesta.body;
                                  break;
                                }catch(e){
                                  return Left(ServidorNoAlcanzado());
                                }
            case 'personal':  try{
                              Uri direccion = Uri.http('hp-api.onrender.com', 'api/characters/staff');
                              var respuesta = await http.get(direccion);
                              json= respuesta.body;
                              break;
                              }catch(e){
                                return Left(ServidorNoAlcanzado());
                              }
            case 'casa-gryffindor': try{
                                      Uri direccion = Uri.http('hp-api.onrender.com', 'api/characters/house/gryffindor');
                                      var respuesta = await http.get(direccion);
                                      json= respuesta.body;
                                      break;
                                    }catch(e){
                                      return Left(ServidorNoAlcanzado());
                                    }
            case 'casa-slytherin': try{
                                      Uri direccion = Uri.http('hp-api.onrender.com', 'api/characters/house/slytherin');
                                      var respuesta = await http.get(direccion);
                                      json= respuesta.body;
                                      break;
                                    }catch(e){
                                      return Left(ServidorNoAlcanzado());
                                    }
            case 'casa-hufflepuff': try{
                                      Uri direccion = Uri.http('hp-api.onrender.com', 'api/characters/house/hufflepuff');
                                      var respuesta = await http.get(direccion);
                                      json= respuesta.body;
                                      break;
                                    }catch(e){
                                        return Left(ServidorNoAlcanzado());
                                    }
            case 'casa-ravenclaw': try{
                                      Uri direccion = Uri.http('hp-api.onrender.com', 'api/characters/house/ravenclaw');
                                      var respuesta = await http.get(direccion);
                                      json= respuesta.body;
                                      break;
                                    }catch(e){
                                        return Left(ServidorNoAlcanzado());
                                    }
            case 'hechizos': try{
                              Uri direccion = Uri.http('hp-api.onrender.com', '/api/spells');
                              var respuesta = await http.get(direccion);
                              json= respuesta.body;
                              break;
                            }catch(e){
                                  return Left(ServidorNoAlcanzado());
                            }
          }
        return Right(json);
  }
}
