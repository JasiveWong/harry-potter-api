import 'dart:convert';

import 'package:fpdart/fpdart.dart';
import 'package:harry_potter/dominio/hechizo.dart';
import 'package:harry_potter/dominio/problemas.dart';

Either<Problema,List<Hechizo>> obtenerListaHechizos(String json){
  try {
    List<dynamic> hechizos = jsonDecode(json);
    return Right(hechizos.map((e) => Hechizo.fromJson(e)).toList());
  } catch (e) {
    return Left(JsonMalformado());
  }
}