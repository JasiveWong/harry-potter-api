import 'package:flutter/material.dart';
import 'package:harry_potter/bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:harry_potter/vistas/vista_creandose.dart';
import 'package:harry_potter/vistas/vista_error.dart';
import 'package:harry_potter/vistas/vista_mostrando_hechizos.dart';
import 'package:harry_potter/vistas/vista_mostrando_menu.dart';
import 'package:harry_potter/vistas/vista_mostrando_opciones_casas.dart';
import 'package:harry_potter/vistas/vista_personajes.dart';
import 'package:harry_potter/vistas/vista_sin_internet.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) {
        BlocVerificacion blocVerificacion = BlocVerificacion();
        Future.delayed(const Duration(seconds: 2),(){
          blocVerificacion.add(Creado());
        });
        return blocVerificacion;
      },
      child: const Aplicacion(),
    );
  }
}

class Aplicacion extends StatelessWidget {
  const Aplicacion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: const Color.fromARGB(255, 220, 208, 192),
        body: Builder( builder: (context) {
          var estado = context.watch<BlocVerificacion>().state;
          if(estado is Creandose){
            return const Center(child: VistaCreandose());
          }
          if(estado is MostrandoMenu){
            return const Center(child: VistaMostrandoMenu());
          }
          if(estado is MostrandoPersonajes){
            return Center(child: VistaMostrandoPersonajes(estado.personajes));
          }
          if(estado is MostrandoEstudiantes){
            return Center(child: VistaMostrandoPersonajes(estado.personajes));
          }
          if(estado is MostrandoPersonal){
            return Center(child: VistaMostrandoPersonajes(estado.personajes));
          }
          if(estado is MostrandoMenuCasas){
            return const Center(child: VistaMostrandoOpcionesCasas());
          }
          if(estado is MostrandoPersonajesPorCasa){
            return Center(child: VistaMostrandoPersonajes(estado.personajes));
          }
          if(estado is MostrandoHechizos){
            return Center(child: VistaMostrandoHechizos(estado.hechizos));
          }
          if(estado is EsperandoInformacion){
            return const Center(child: VistaCreandose());
          }
          if(estado is MostrandoSinInternet){
            return const Center(child: VistaSinInternet());
          }
          if(estado is MostrandoError){
            return const Center(child: VistaError());
          }
          return const Center(
            child: Text('Algo salio mal'),
          );
        }),
      ),
    );
  }
}
