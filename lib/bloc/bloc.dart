import 'package:bloc/bloc.dart';
import 'package:harry_potter/dominio/hechizo.dart';
import 'package:harry_potter/dominio/personaje.dart';
import 'package:harry_potter/dominio/problemas.dart';
import 'package:harry_potter/repositorio/repositorio_hechizo.dart';
import 'package:harry_potter/repositorio/repositorio_json.dart';
import 'package:harry_potter/repositorio/repositorio_personaje.dart';

class Evento{}

class Estado{}

class Creandose extends Estado {}

class MostrandoMenu extends Estado {}

class EsperandoInformacion extends Estado {}

class MostrandoSinInternet extends Estado {}

class MostrandoError extends Estado {}

class MostrandoPersonajes extends Estado {
  final List<Personaje> personajes;
  MostrandoPersonajes(this.personajes);
}

class MostrandoEstudiantes extends Estado {
  final List<Personaje> personajes;
  MostrandoEstudiantes(this.personajes);
}

class MostrandoPersonal extends Estado {
  final List<Personaje> personajes;
  MostrandoPersonal(this.personajes);
}

class MostrandoMenuCasas extends Estado {}

class MostrandoPersonajesPorCasa extends Estado {
  final List<Personaje> personajes;
  MostrandoPersonajesPorCasa(this.personajes);
}

class MostrandoHechizos extends Estado {
  final List<Hechizo> hechizos;
  MostrandoHechizos(this.hechizos);
}

class Creado extends Evento {}

class OpcionRecibida extends Evento {
  final String opcion;
  OpcionRecibida(this.opcion);
}

class OpcionCasaRecibida extends Evento {
  final String opcion;
  OpcionCasaRecibida(this.opcion);
}

class BlocVerificacion extends Bloc<Evento, Estado> {
  RepositorioOnlineJson repositorio = RepositorioOnlineJson();
  BlocVerificacion() : super(Creandose()){
    on<Creado>((event, emit) {
      emit(MostrandoMenu()); 
    });
    on<OpcionRecibida>((event, emit)async{
    emit(EsperandoInformacion());
        switch (event.opcion) {
          case 'personajes':  final respuesta = await repositorio.obtenerjson(event.opcion);
                              respuesta.match((l){
                                if(l is ServidorNoAlcanzado){
                                  emit(MostrandoSinInternet());
                                }else{
                                  emit(MostrandoError());
                                }
                              }, (r){
                                final respuestaPersonajes = obtenerListaPersonajes(r);
                                respuestaPersonajes.match((l) {
                                  emit(MostrandoError());
                                }, (r) {
                                  emit(MostrandoPersonajes(r));
                                });
                              });
                              break;
          case 'estudiantes': final respuesta = await repositorio.obtenerjson(event.opcion);
                              respuesta.match((l){
                                if(l is ServidorNoAlcanzado){
                                  emit(MostrandoSinInternet());
                                }else{
                                  emit(MostrandoError());
                                }
                              }, (r){
                                final respuestaPersonajes = obtenerListaPersonajes(r);
                                respuestaPersonajes.match((l) {
                                  emit(MostrandoError());
                                }, (r) {
                                  emit(MostrandoEstudiantes(r));
                                });
                              });
                              break;
          case 'personal':    final respuesta = await repositorio.obtenerjson(event.opcion);
                              respuesta.match((l){
                                if(l is ServidorNoAlcanzado){
                                  emit(MostrandoSinInternet());
                                }else{
                                  emit(MostrandoError());
                                }
                              }, (r){
                                final respuestaPersonajes = obtenerListaPersonajes(r);
                                respuestaPersonajes.match((l) {
                                  emit(MostrandoError());
                                }, (r) {
                                  emit(MostrandoPersonal(r));
                                });
                              });
                              break;
          case 'porcasa':     emit(MostrandoMenuCasas());
                              break;
          case 'hechizos':    final respuesta = await repositorio.obtenerjson(event.opcion);
                              respuesta.match((l){
                                if(l is ServidorNoAlcanzado){
                                  emit(MostrandoSinInternet());
                                }else{
                                  emit(MostrandoError());
                                }
                              }, (r){
                                final respuestaHechizos = obtenerListaHechizos(r);
                                respuestaHechizos.match((l) {
                                  emit(MostrandoError());
                                }, (r) {
                                  emit(MostrandoHechizos(r));
                                });
                              });
                              break;
          default:
        }
    });
    on<OpcionCasaRecibida>((event, emit) async{
      emit(EsperandoInformacion());
      final respuesta = await repositorio.obtenerjson(event.opcion);
      respuesta.match((l){
        if(l is ServidorNoAlcanzado){
          emit(MostrandoSinInternet());
        }else{
          emit(MostrandoError());
        }
      }, (r){
        final respuestaPersonajes = obtenerListaPersonajes(r);
        respuestaPersonajes.match((l) {
          emit(MostrandoError());
        }, (r) {
          emit(MostrandoPersonajesPorCasa(r));
        });
      });
    });
  }
}

