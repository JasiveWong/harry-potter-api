import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:harry_potter/bloc/bloc.dart';
import 'package:harry_potter/dominio/personaje.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class VistaMostrandoPersonajes extends StatelessWidget {
  final List<Personaje> personajes;
  const VistaMostrandoPersonajes(this.personajes,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final b = context.read<BlocVerificacion>();
    return Column(
      children: [
        const SizedBox(height: 10),
        const Image(image: AssetImage('assets/logo.png'), width: 300),
        const Text("Lista de personajes",
        style: TextStyle(fontSize: 30)),
        ElevatedButton(onPressed: (){
            b.add(Creado());
        }, style: ElevatedButton.styleFrom(
            primary: Colors.amberAccent,
            onPrimary: Colors.black,
          ), child: const Text('Volver')),
        Expanded(
          child: ListView.builder(
                  itemCount: personajes.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                        title: Text(personajes[index].nombre),
                        subtitle: Text('Actor: ${personajes[index].actor}'),
                        leading: personajes[index].imagen.isEmpty ? 
                        const Icon(Icons.person):CachedNetworkImage(imageUrl: personajes[index].imagen, placeholder: (context, url) => const CircularProgressIndicator(),
                        errorWidget: (context, url, error) => const Icon(Icons.error)),
                        onTap: () {
                          AlertDialog alert = AlertDialog(
                          title: Text(personajes[index].nombre, style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 30)),
                          content: SingleChildScrollView(
                          child: Column(
                            children: [
                              personajes[index].imagen.isEmpty ? 
                                CachedNetworkImage(imageUrl: 'https://definicion.de/wp-content/uploads/2019/07/perfil-de-usuario.png',placeholder: (context, url) => const CircularProgressIndicator(),
                              errorWidget: (context, url, error) => const Icon(Icons.error)):CachedNetworkImage(imageUrl: personajes[index].imagen, placeholder: (context, url) => const CircularProgressIndicator(),
                              errorWidget: (context, url, error) => const Icon(Icons.error),),
                              Text('Especie: ${personajes[index].especie}', style: const TextStyle(fontSize: 20)),
                              Text('Genero: ${personajes[index].genero}', style: const TextStyle(fontSize: 20)),
                              Text('Casa: ${personajes[index].casa}', style: const TextStyle(fontSize: 20)),
                              Text('Fecha de nacimiento: ${personajes[index].fechaNacimiento}', style: const TextStyle(fontSize: 20)),
                              Text('Mago: ${personajes[index].esMago.toString()}', style: const TextStyle(fontSize: 20)),
                              Text('Ascendencia: ${personajes[index].ascendencia}', style: const TextStyle(fontSize: 20)),
                              Text('estudiante de Hogwarts: ${personajes[index].estudianteHogwarts}', style: const TextStyle(fontSize: 20)),
                              Text('Personal de Hogwarts: ${personajes[index].personalHogwarts}', style: const TextStyle(fontSize: 20)),
                              Text('Patronus: ${personajes[index].patronus}', style: const TextStyle(fontSize: 20)),
                              Text('Actor: ${personajes[index].actor}', style: const TextStyle(fontSize: 20)),
                            ],
                          ),
                        ),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(context),
                            child: const Text('Cerrar', style: TextStyle(color: Colors.red)),
                          ),
                        ],
                        );
                        
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return alert;
                          },
                        );
                      },
                    );
                  },
              ),
        )
      ],
    );
  }
}
