import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:harry_potter/bloc/bloc.dart';

class VistaMostrandoOpcionesCasas extends StatelessWidget {
  const VistaMostrandoOpcionesCasas({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final b = context.read<BlocVerificacion>();
    return Column(
      children: [
        const SizedBox(height: 10),
        const Image(image: AssetImage('assets/logo.png'), width: 300),
        const SizedBox(height: 30),
        const Text("Selecciona una casa",
        style: TextStyle(fontSize: 30)),
        const SizedBox(height: 30),
        ElevatedButton(
        onPressed: (() {
          b.add(OpcionCasaRecibida('casa-gryffindor'));
        }),
        style: TextButton.styleFrom(
                primary: Colors.white,
                backgroundColor: const Color.fromARGB(255, 175, 47, 47)),
        child: const Text("Gryffindor", style: TextStyle(fontSize: 15))),
        const SizedBox(height: 30),
        ElevatedButton(
        onPressed: (() {
          b.add(OpcionCasaRecibida('casa-slytherin'));
        }),
        style: TextButton.styleFrom(
                primary: Colors.white,
                backgroundColor: const Color.fromARGB(255, 23, 133, 23)),
        child: const Text("Slytherin", style: TextStyle(fontSize: 15))),
        const SizedBox(height: 30),
        ElevatedButton(
        onPressed: (() {
          b.add(OpcionCasaRecibida('casa-hufflepuff'));
        }),
        style: TextButton.styleFrom(
                primary: Colors.white,
                backgroundColor: const Color.fromARGB(255, 219, 201, 39)),
        child: const Text("Hufflepuff", style: TextStyle(fontSize: 15))),
        const SizedBox(height: 30),
        ElevatedButton(
        onPressed: (() {
          b.add(OpcionCasaRecibida('casa-ravenclaw'));
        }),
        style: TextButton.styleFrom(
                primary: Colors.white,
                backgroundColor: const Color.fromARGB(255, 31, 46, 131)),
        child: const Text("Ravenclaw", style: TextStyle(fontSize: 15))),
      ],
    );
  }
}