import 'package:flutter/material.dart';
import 'package:harry_potter/bloc/bloc.dart';
import 'package:harry_potter/dominio/hechizo.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class VistaMostrandoHechizos extends StatelessWidget {
  final List<Hechizo> hechizos;
  const VistaMostrandoHechizos(this.hechizos,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final b = context.read<BlocVerificacion>();
    return Column(
      children: [
        const SizedBox(height: 10),
        const Image(image: AssetImage('assets/logo.png'), width: 300),
        const Text("Lista de hechizos",
        style: TextStyle(fontSize: 30)),
        ElevatedButton(onPressed: (){
            b.add(Creado());
        }, style: ElevatedButton.styleFrom(
            primary: Colors.amberAccent,
            onPrimary: Colors.black,
          ), child: const Text('Volver')),
        Expanded(
          child: ListView.builder(
                  itemCount: hechizos.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                        title: Text(hechizos[index].nombre),
                        subtitle: Text('Descripcion: ${hechizos[index].descripcion}'),
                    );
                  },
              ),
        ),
      ]
    );
}
}
