import 'package:flutter/material.dart';

class VistaCreandose extends StatelessWidget {
  const VistaCreandose({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: const [
        Image(image: AssetImage('assets/logo.png'), width: 300),
        Text("Cargando, espere un momento...",
        style: TextStyle(fontSize: 30)),
        SizedBox(
          width: 100,
          height: 100,
          child: Center(child: LinearProgressIndicator(backgroundColor: Colors.amberAccent, color: Colors.amber)),
        ),
      ],
    );
  }
}