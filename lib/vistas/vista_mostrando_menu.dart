import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:harry_potter/bloc/bloc.dart';

class VistaMostrandoMenu extends StatelessWidget {
  const VistaMostrandoMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final b = context.read<BlocVerificacion>();
    return Column(
      children: [
        const SizedBox(height: 10),
        const Image(image: AssetImage('assets/logo.png'), width: 300),
        const Text("¿Qué lista deseas ver?",
        style: TextStyle(fontSize: 30)),
        const SizedBox(height: 30),
        ElevatedButton.icon(
              icon: const Icon(
                Icons.supervised_user_circle_sharp,
                size: 20,
              ),
              onPressed: () {
                b.add(OpcionRecibida('personajes'));
              },
              style: TextButton.styleFrom(
                primary: Colors.black,
                backgroundColor: Colors.amberAccent),
                label: const Text("Todos los personajes")
            ),
        const SizedBox(height: 30),
        ElevatedButton.icon(
              icon: const Icon(
                Icons.school,
                size: 20,
              ),
              onPressed: () {
                b.add(OpcionRecibida('estudiantes'));
              },
              style: TextButton.styleFrom(
                primary: Colors.black,
                backgroundColor: Colors.amberAccent),
                label: const Text("Estudiantes de Hogwarts")
            ),
        const SizedBox(height: 30),
        ElevatedButton.icon(
              icon: const Icon(
                Icons.work,
                size: 20,
              ),
              onPressed: () {
                b.add(OpcionRecibida('personal'));
              },
              style: TextButton.styleFrom(
                primary: Colors.black,
                backgroundColor: Colors.amberAccent),
                label: const Text("Personal de Hogwarts")
            ),
        const SizedBox(height: 30),
        ElevatedButton.icon(
              icon: const Icon(
                Icons.house,
                size: 20,
              ),
              onPressed: () {
                b.add(OpcionRecibida('porcasa'));
              },
              style: TextButton.styleFrom(
                primary: Colors.black,
                backgroundColor: Colors.amberAccent),
                label: const Text("Personajes por casa")
            ),
        const SizedBox(height: 30),
        ElevatedButton.icon(
              icon: const Icon(
                Icons.auto_awesome,
                size: 20,
              ),
              onPressed: () {
                b.add(OpcionRecibida('hechizos'));
              },
              style: TextButton.styleFrom(
                primary: Colors.black,
                backgroundColor: Colors.amberAccent),
                label: const Text("Todos los hechizos")
            ),
      ],
    );
  }
}