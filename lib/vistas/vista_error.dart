import 'package:flutter/material.dart';

class VistaError extends StatelessWidget {
  const VistaError({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: const [
        Icon(Icons.error_outline, size: 70, color: Color.fromARGB(255, 228, 35, 35),),
        Text('Ha ocurrido un error', style: TextStyle(fontSize: 35)),
      ],
    );
  }
}