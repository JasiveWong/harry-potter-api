import 'package:flutter/material.dart';

class VistaSinInternet extends StatelessWidget {
  const VistaSinInternet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: const [
        Image(image: AssetImage('assets/dino.png')),
        Text('No hay internet, configura versión offline y continua', style: TextStyle(fontSize: 20)),
      ],
    );
  }
}